﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 游戏基础管理器 todo: 暂时没用
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 游戏基础管理器
    /// </summary>
    public class BaseManager //: IManager
    {
        public virtual void Init()
        {

        }

        public virtual void Remove()
        {
            
        }

    }


    //public class TestMgr : BaseManager
    //{
    //    public void TestLog()
    //    {
    //        Debug.Log("MyTestMgr");
    //    }
    //}


    //public class TT
    //{
    //    void TE()
    //    {
    //        TestMgr.Ins.TestLog();
    //    }
    //}
}
