﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 数据模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 数据模块,包括：
    ///     1.数据库
    ///     2.读表
    ///     3.大多数特殊的数据
    ///     4.其他数据
    ///     5.数据的序列化和反序列化
    /// </summary>
    public class DataModule : BaseModule
    {
        public DataModule(string name = ModuleName.Data) : base(name)
        {
            
        }

        public override void InitGame()
        {
            
        }
    }
}
