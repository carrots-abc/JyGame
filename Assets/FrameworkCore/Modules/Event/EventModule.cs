﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 事件模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 事件模块
    /// </summary>
    public class EventModule : BaseModule
    {
        public EventModule(string name = ModuleName.Event):base(name)
        {

        }

        /// <summary>
        /// 游戏生命周期： 初始化
        /// </summary>
        public override void InitGame()
        {
            MessageCtrl.CreateSingleton();
        }
    }
}
