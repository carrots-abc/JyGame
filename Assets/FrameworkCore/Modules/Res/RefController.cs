﻿/*
 *  date: 2018-05-17
 *  author: John-chen
 *  cn: 引用计数控制器
 *  en: todo:
 */

using UnityEngine;
using System.Collections.Generic;

namespace JyFramework
{ 
    /// <summary>
    /// 引用计数控制器
    /// </summary>
    public class RefController : Singleton<RefController>
    {
        /// <summary>
        /// 移除本类
        /// </summary>
        public override void Remove()
        {
            MessageCtrl.Ins.RemoveCtrl(_refEventCtrl);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            _refObjs = new Dictionary<string, RefAssetBundle>();

            _refEventCtrl = new EventController("RefEventController");
            MessageCtrl.Ins.AddEventCtrl(_refEventCtrl);

            _refEventCtrl.RegistEvent(ResEvent.OnAddRef, OnAddRefCallBack);
            _refEventCtrl.RegistEvent(ResEvent.OnReduceRef, OnReduceRefCallBack);
            _refEventCtrl.RegistEvent(ResEvent.AddAssetBundleRes, OnAddAssetBundleRes);
            _refEventCtrl.RegistEvent(ResEvent.DestoryRefAssetBundle, OnDestoryAssetBundleRes);
        }

        /// <summary>
        /// 加载成功
        /// </summary>
        /// <param name="args"></param>
        private void OnAddAssetBundleRes(object[] args)
        {
            string resName = (string) args[0];
            AssetBundle abRes = args[1] as AssetBundle;

            RefAssetBundle refAb = new RefAssetBundle(resName, abRes);

            if (_refObjs.ContainsKey(resName))
            {
                _refObjs[resName] = refAb;
            }
            else
            {
                _refObjs.Add(resName, refAb);
            }

        }

        /// <summary>
        /// 只有添加了引用计数脚本后才做引用计数
        /// </summary>
        /// <param name="args"></param>
        private void OnAddRefCallBack(object[] args)
        {
            string resName = (string)args[0];

            RefAssetBundle refObj = null;
            if (_refObjs.TryGetValue(resName, out refObj))
                refObj.AddRef();
            else
            {
                Debug.Log(string.Format("无路径为： <{0}> 的资源！", resName));
            }
        }

        /// <summary>
        /// 引用减少
        /// </summary>
        /// <param name="args"></param>
        private void OnReduceRefCallBack(object[] args)
        {
            string resName = (string)args[0];

            RefAssetBundle refObj = null;
            if (_refObjs.TryGetValue(resName, out refObj))
            {
                refObj.DelRef();
            }
        }

        /// <summary>
        /// 删除本资源时引用
        /// </summary>
        /// <param name="args"></param>
        private void OnDestoryAssetBundleRes(object[] args)
        {
            string resName = (string)args[0];

            RefAssetBundle refObj = null;
            if (_refObjs.TryGetValue(resName, out refObj))
            {
                _refObjs.Remove(resName);
            }
        }

        private Dictionary<string, RefAssetBundle> _refObjs;
        private EventController _refEventCtrl;
    }
}
